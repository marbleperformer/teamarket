from django.db import models

class Customer(models.Model):
	name = models.CharField(max_length=150, verbose_name='Имя')
	surname = models.CharField(max_length=150, verbose_name='Фамилия')
	patronymic = models.CharField(max_length=150, verbose_name='Отчество')
	phone = models.CharField(max_length=100, verbose_name='Номер телофона')
	email = models.CharField(max_length=150, verbose_name='Электронная почта')
	address = models.TextField(verbose_name='Адрес')

	class Meta:
		verbose_name = 'Покупатель'
		verbose_name_plural = 'Покупатели'
