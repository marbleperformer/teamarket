# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-15 08:35
from __future__ import unicode_literals

from django.db import migrations
import wagtail.wagtailcore.blocks
import wagtail.wagtailcore.fields
import wagtail.wagtailimages.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0007_auto_20170414_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infopage',
            name='body',
            field=wagtail.wagtailcore.fields.StreamField((('Текст', wagtail.wagtailcore.blocks.RichTextBlock()), ('Партнеры', wagtail.wagtailcore.blocks.ListBlock(wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Название')), ('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('url', wagtail.wagtailcore.blocks.URLBlock(label='Ссылка', required=False)))), template='blocks/partner_list_block.html'))), verbose_name='Содержимое'),
        ),
    ]
