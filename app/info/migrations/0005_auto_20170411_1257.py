# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-11 05:57
from __future__ import unicode_literals

from django.db import migrations
import wagtail.wagtailcore.blocks
import wagtail.wagtailcore.fields
import wagtail.wagtailimages.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0004_auto_20170411_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='infopage',
            name='body',
            field=wagtail.wagtailcore.fields.StreamField((('Текст', wagtail.wagtailcore.blocks.RichTextBlock()), ('Партнеры', wagtail.wagtailcore.blocks.ListBlock(wagtail.wagtailcore.blocks.StructBlock((('title', wagtail.wagtailcore.blocks.CharBlock(label='Название')), ('image', wagtail.wagtailimages.blocks.ImageChooserBlock(label='Изображение')), ('url', wagtail.wagtailcore.blocks.URLBlock(label='Ссылка', required=False))))))), verbose_name='Содержимое'),
        ),
    ]
