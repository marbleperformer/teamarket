from django.db import models

from wagtail.wagtailcore import blocks

from wagtail.wagtailcore.models import Page

from wagtail.wagtailcore.fields import RichTextField, StreamField

from wagtail.wagtailadmin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel

from wagtail.wagtailimages.blocks import ImageChooserBlock

class PartnerBlock(blocks.StructBlock):
	title = blocks.CharBlock(label='Название')
	image =  ImageChooserBlock(label='Изображение')
	url = blocks.URLBlock(label='Ссылка', required=False)

	class Meta:
		template = 'blocks/partner_block.html'

class InfoPage(Page):
	h1_title = models.CharField(max_length=250, verbose_name='H1 страницы', blank=True, null=True)

	keywords = models.CharField(max_length=250, verbose_name='Ключевые слова', blank=True, null=True)

	body = StreamField([
			('Текст', blocks.RichTextBlock()),
			('Партнеры', blocks.ListBlock(PartnerBlock(), template='blocks/partner_list_block.html'))
		], verbose_name='Содержимое')

	content_panels = Page.content_panels + [
		StreamFieldPanel('body')
	]

	promote_panels = [
		MultiFieldPanel([
			FieldPanel('slug'),
			FieldPanel('h1_title'),
			FieldPanel('keywords'),
			FieldPanel('seo_title'),
			FieldPanel('show_in_menus'),
			FieldPanel('search_description'),
		], 'Общие настройки страниц')
	]

	parent_page_types = [
		'home.HomePage'
	]

	subpage_types = [
	]

	class Meta:
		verbose_name = 'Информация'
		verbose_name_plural = 'Информация'

# PATCH TODO

# class Group(models.Model):
# 	title = models.CharField(max_length=150, verbose_name='Название')

# 	class Meta:
# 		verbose_name = 'Информационный раздел'
# 		verbose_name = 'Информационные разделы'

# class Info(models.Model):
# 	TYPE_LIST = (
# 		('IMG', 'Изображение'),
# 		('IMG', 'Изображение'),
# 	)

# 	group = models.Foreignkey(Group, verbose_name='Информационный раздел') 
