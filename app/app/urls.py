from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from search import views as search_views
from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtailcore import urls as wagtail_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls

from wagtail.wagtailimages.views.serve import ServeView

from wagtail.api.v2.endpoints import PagesAPIEndpoint
from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.wagtailimages.api.v2.endpoints import ImagesAPIEndpoint

from wagtail.contrib.wagtailsitemaps.views import sitemap

from rest_framework.routers import DefaultRouter

from catalog.views import FilterViewSet

# from teas.views import TeaViewSet, GroupViewSet

router = DefaultRouter()
# router.register('teas', TeaViewSet)
# router.register('groups', GroupViewSet)

# router.register('filters',)

router.register('filters', FilterViewSet)

# router.register('pages', PagesAPIEndpoint)
wagtail_router = WagtailAPIRouter('wagtailapi')

wagtail_router.register_endpoint('pages', PagesAPIEndpoint)
wagtail_router.register_endpoint('images', ImagesAPIEndpoint)

urlpatterns = [
    url(r'^images/([^/]*)/(\d*)/([^/]*)/[^/]*$', ServeView.as_view(), name='wagtailimages_serve'),

    url(r'^api/', include(router.urls)),
    url(r'^api/v1/', include(wagtail_router.urls)),
    # url(r'^teas/', include('teas.urls')),
    url(r'^payment/', include('cart.urls')),
    # url(r'^orders/', include('orders.urls')),
    url(r'^django-admin/', include(admin.site.urls)),

    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),

    url(r'^search/$', search_views.search, name='search'),

    url(r'^sitemap\.xml$', sitemap),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    url(r'', include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    url(r'^pages/', include(wagtail_urls)),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
