var CartView = Backbone.View.extend({
    el:'.cartpage',
    template: _.template($('#cartTemplate').html()),

    // cart_length: 0,

    events: {
        'click .btn-buy': 'add_cart_item',
    },

    initialize: function() {
        cart_cookies = Cookies.get('cart_amount')
        if (!cart_cookies) {
            Cookies.set('cart_amount')
            cart_cookies = '[]'
        }

        // this.cart_length = 0

        cart_amount = JSON.parse(cart_cookies)
        cart_amount.forEach(function(el) {
            cart_collection.add(el)
            // console.log(el)
            // this.cart_length = this.cart_length + el.amount
            // console.log(this.cart_length)
        })
        this.listenTo(cart_collection, 'change:amount', this.set_cart_length)
        this.listenTo(cart_collection, 'remove', this.set_cart_length)

        $(window).scroll(this.set_menu_fixed)


        this.render()
    },

    set_cart_length: function(el) {
        cart_length = _.reduce(cart_collection.toJSON(), function(memo, num){return memo + num.amount}, 0)
        Cookies.set('cart_amount', JSON.stringify(cart_collection))
        this.$el.find('.badge').html(cart_length)
        // this.$el.each(function(idx, el){
        //     console.log(this)
        //     $(el).find('.badge').html(this.cart_length)
        // })
    },

    set_menu_fixed: function(el) {
        scroll_height = $(window).scrollTop()
        if (scroll_height >= 0 && scroll_height <= 125){
            if ($('.nav.navbar-nav').hasClass('fixed')){
                $('.nav.navbar-nav').removeClass('fixed')
            }
        } else {
            if (!$('.nav.navbar-nav').hasClass('fixed')){
                $('.nav.navbar-nav').addClass('fixed')
            }
        }
    },

    render: function() {
        // console.log(this.cart_length)
        cart_length = _.reduce(cart_collection.toJSON(), function(memo, num){return memo + num.amount}, 0)
        cart_template = this.template({length:cart_length})
        this.$el.html(cart_template)
        // this.$el.each(function(index, element) {
        //     $(element).html(cart_template)
        // })
        return this
    }
})

cart_view = new CartView()
