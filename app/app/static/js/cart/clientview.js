var ClientView = Backbone.View.extend({
	el:'#mainPlaceholder',
	template:_.template($('#clientViewTemplate').html()),

	events:{
		'click .btn-buy': 'set_customer_cookies'
	},

	set_customer_cookies: function() {
		info = {
			surname: $('#client_surname').val(),
			name: $('#client_name').val(),
			patronymic: $('#client_patronymic').val(),
			phone: $('#client_phone').val(),
			email: $('#client_email').val(),
			address: $('#client_address').val()
		}
		Cookies.set('customer_info', info)
	},

	render: function() {
		this.$el.html(
			this.template()
		)
	}
})