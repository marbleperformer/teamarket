from __future__ import absolute_import, unicode_literals

from .base import *

DEBUG = False

SECRET_KEY = ')8cjvr3)13biaq3u7l7l)f0r2*jtrks=va#3xb%h!_1+-lw(gh'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'teamarket',
        'USER': 'postgres',
        'PASSWORD': 'M15n06b67',
        'HOST':'localhost',
        'PORT':'',
    }
}

try:
    from .local import *
except ImportError:
    pass
