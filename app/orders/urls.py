from django.conf.urls import url

from .views import SuccessView

app = 'orders'

urlpatterns = [
	url(r'^(?P<pk>[0-9]+)/success/$', SuccessView.as_view(), name='success'),
]
