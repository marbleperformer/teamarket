from django.db import models

class Order(models.Model):
	teas = models.ManyToManyField('teas.tea', verbose_name='Чай')

	cps_context_id = models.CharField(max_length=250)

	is_open = models.BooleanField(verbose_name='Открыт', default=True)

	created = models.DateTimeField(auto_now_add=True)
	modified = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Заказ'
		verbose_name_plural = 'Заказы'
