import smtplib, urllib, json

from django.shortcuts import render, redirect

from django.views.generic import View

from django.conf import settings

from django.contrib.auth.models import Group as UserGroup

from email.mime.text import MIMEText

from .mixins import OrderExistRequired, OrderIsOpenRequired

from .models import Order

from mails.models import Mail, Group


class SuccessView(OrderExistRequired, OrderIsOpenRequired, View):
	success_page_url = '/success/'

	order_closed_url = '/cart/'
	order_not_exist_url = '/cart/'

	order = None

	def get_caontent_from_cookies(self, request):
		quote_cart_cookies = request.COOKIES.get('cart_items')
		cart_cookies_string = urllib.parse.unquote(quote_cart_cookies)
		cart_cookies = json.loads(cart_cookies_string)

		tea_list = list()
		for idx, item in enumerate(cart_cookies):
			params = list()
			params.append(str(idx + 1) + '.')
			params.append(str(item['item']['title']))
			params.append(str(item['items']) + 'шт.')
			cost = item['item']['sale'] if item['item']['sale'] else item['item']['cost']
			params.append('по ' + str(cost) + ' руб.')
			tea_list.append(' '.join(params))
		items_cost = sum([
			float(item['item']['sale'] if item['item']['sale'] else item['item']['cost']) * float(item['items']) 
			for item in cart_cookies
		])
		tea_list.append('Итого ' + str(items_cost))
		return '\n'.join(tea_list)

	def format_customer_message(self, request):
		quote_customer_cookies = request.COOKIES.get('customer_info')
		customer_cookies_string = urllib.parse.unquote(quote_customer_cookies)
		customer_cookies = json.loads(customer_cookies_string)

		mail_template = Mail.objects.get(group=1)

		message_text = mail_template.body

		message_content = self.get_caontent_from_cookies(request)

		message = MIMEText('\n\n'.join([message_text, message_content]), _subtype='plain', _charset='utf-8')
		message['From'] = 'support@kitai-ryadom.ru'
		message['To'] = customer_cookies['email']

		return message

	def format_moderators_message(self, request):
		md_group = UserGroup.objects.get(name='Moderators')

		mails = [user.email for user in md_group.user_set.all()]

		message_content = self.get_caontent_from_cookies(request)

		quote_customer_cookies = request.COOKIES.get('customer_info')
		customer_cookies_string = urllib.parse.unquote(quote_customer_cookies)
		customer_cookies = json.loads(customer_cookies_string)

		params = ['Покупатель:']
		params.append('Фамилия: {}'.format(customer_cookies['surname']))
		params.append('Имя: {}'.format(customer_cookies['name']))
		params.append('Отчество: {}'.format(customer_cookies['patronymic']))

		params.append('Контактная информация')
		params.append('Номер телефона: {}'.format(customer_cookies['phone']))
		params.append('Электронная почта: {}'.format(customer_cookies['email']))
		params.append('Адрес: {}'.format(customer_cookies['address']))

		message_text = '\n'.join(params)

		message = MIMEText('\n\n'.join([message_content, message_text]), _subtype='plain', _charset='utf-8')

		message['Subject'] = 'Поступила заявка с сайта Китай рядом!'
		message['From'] = 'support@kitai-ryadom.ru'
		message['To'] = str(mails)

		return message

	def get(self, request, pk, **kwargs):
		self.order = Order.objects.get(id=pk)

		server = smtplib.SMTP_SSL('smtp.yandex.ru:465')
		server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)

		md_group = UserGroup.objects.get(name='Moderators')

		mails = [user.email for user in md_group.user_set.all()]

		moderators_message = self.format_moderators_message(request)

		customer_message = self.format_customer_message(request)

		server.sendmail(settings.EMAIL_HOST_USER, mails, moderators_message.as_string())	
		server.sendmail(settings.EMAIL_HOST_USER, mails, customer_message.as_string())	

		server.quit()

		self.order.is_open = False
		self.order.save()

		return redirect(self.success_page_url)

class FailView(OrderExistRequired, OrderIsOpenRequired, View):
	fail_page_url = '/success/'
	order_closed_url = '/cart/'
	order_not_exist_url = '/cart/'

	def get(self, request, pk, **kwargs):
		self.order = Order.objects.get(id=pk)

		self.order.is_open = False
		self.order.save()

		return redirect(self.fail_page_url)
