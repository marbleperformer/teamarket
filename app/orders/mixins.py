from django.shortcuts import redirect

from .models import Order

class OrderExistRequired(object):
	order_not_exist_url = ''

	def dispatch(self, reqiest, *args, **kwargs):
		order = Order.objects.get(id=kwargs['pk'])
		if not order.is_open:
			return redirect(self.order_not_exist_url)
		return super(OrderExistRequired, self).dispatch(reqiest, *args, **kwargs)

class OrderIsOpenRequired(object):
	order_closed_url = ''

	def dispatch(self, reqiest, *args, **kwargs):
		order = Order.objects.get(id=kwargs['pk'])
		if not order:
			return redirect(self.order_closed_url)
		return super(OrderIsOpenRequired, self).dispatch(reqiest, *args, **kwargs)
