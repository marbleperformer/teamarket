var PaginationView = Backbone.View.extend({
	el:'#tilepagination',
	body_el: '.pagination-body',
	template: _.template($('#paginationTemplate').html()),
	item_template: _.template($('#paginationItemTemplate').html()),

	collection: null,

	events: {
		'click .btn-page': 'navigate_to_page',
	},

	initialize:function(){
		this.listenToOnce(this.collection, 'sync', this.render)
	},

	navigate_to_page: function() {
		$("body, html").animate({
			scrollTop: $("body").position().top
		})
	},

	render: function() {
		this.$el.empty()
		page = this.collection.page ? this.collection.page : 0
		filters_url = this.collection.filters ? '/filters/' + this.collection.filters : ''

		pages = Math.ceil(this.collection.count/ item_api_limit)
		if (pages>1) {
			next_class = page >= pages - 1 ? 'hidden' : ''

			prev_class = page <= 0 ? 'hidden' : ''

			next_url =  filters_url + '/page/' + (parseInt(page) + 1)
			prev_url = filters_url + '/page/' + (parseInt(page) - 1)

			this.$el.html(
				this.template({
					prev_class: prev_class,
					next_class: next_class,
					next_url:next_url,
					prev_url:prev_url
				})
			)

			for (var idx = 0; idx < pages; idx++) {
				number = idx + 1

				url = filters_url + '/page/' + idx

				is_active = page == idx ? 'active' : ''

				$(this.body_el).append(
					this.item_template({
						url:url, number:number, is_active:is_active
					})
				)
			}
		}
		return this
	}

})