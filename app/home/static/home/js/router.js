var MainRouter = Backbone.Router.extend({
	// url: tea_api_url + '&limit=' + tea_collection.limit,

	routes: {
		'(/)': 'all',
		'(/)page/:page': 'all',
		'(/)filters/:filters': 'filter',
		'(/)filters/:filters/page/:page': 'filter',
	},

	initialize: function() {
		this.item_collection = new ItemCollection()
		this.filter_collection = new FilterCollection()
	},

	all: function(page) {
		this.item_collection.page = page

		this.item_collection.filters = ''

		this.filter_collection.filters = ''

		item_list_view = new ItemTileView({collection:this.item_collection})

		filter_list_view = new FilterListView({collection:this.filter_collection})

		pagination_view = new PaginationView({collection: this.item_collection})
	},

	filter: function(filters, page) {
		this.item_collection.page = page

		this.item_collection.filters = filters

		this.filter_collection.filters = filters

		item_list_view = new ItemTileView({collection:this.item_collection})

		filter_list_view = new FilterListView({collection:this.filter_collection})

		pagination_view = new PaginationView({collection: this.item_collection})
	},
})

router = new MainRouter()

Backbone.history.start()