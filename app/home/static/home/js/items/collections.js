var ItemCollection = Backbone.Collection.extend({
	page: 0,
	filters: null,

	url: function() {
		item_page_url = parseInt(this.page) > 0 ? '&offset=' + (item_api_limit * parseInt(this.page)) : ''
		item_filter_url = this.filters ? this.filters : ''
		return item_api_url + item_filter_url + item_page_url
	},

	parse: function(response) {
		this.count = response.meta.total_count
		return response.items
	}
})