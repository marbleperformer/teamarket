var GroupCollection = Backbone.Collection.extend({
	url: '/api/groups'
})

var GroupItemView = Backbone.View.extend({
	className: 'filter-item row',
	template: _.template($('#filterItemtemplate').html()),

	render: function() {
		this.$el.html(
			this.template(this.model.toJSON())
		)
		return this
	}
})

var GroupListView = Backbone.View.extend({
	el:'#filterItems',
	tagName:'div',
	collection: new GroupCollection(),

	initialize: function() {
		this.collection.fetch()
		this.listenTo(this.collection, 'sync', this.render)
	},

	render: function() {
		this.collection.each((item) => {
			item_view = new GroupItemView({model:item})
			this.$el.append(item_view.render().el)
		})
		return this
	}
})

group_view = new GroupListView()
