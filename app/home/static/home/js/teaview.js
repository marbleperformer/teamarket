var Tea = Backbone.Model.extend({})

var TeaCollection = Backbone.Collection.extend({
	model: Tea,
	url: '/api/teas'
})

var TeaItemView = Backbone.View.extend({
	className: 'tile-item col-md-4 col-sm-6 col-xs-12',
	template: _.template($('#tileItemTemplate').html()),

	render: function() {
		this.$el.html(
			this.template(this.model.toJSON())
		)
		return this
	}

})

var TeaTileView = Backbone.View.extend({
	el:'#tileItems',
	collection: new TeaCollection(),

	initialize: function(){
		this.collection.fetch()
		this.listenTo(this.collection, 'sync', this.render)
	},

	render: function(){
		this.collection.each((item) => {
			item_view = new TeaItemView({model:item})
			this.$el.append(item_view.render().el)
		}, this)
		return this
	}
})

view = new TeaTileView()
