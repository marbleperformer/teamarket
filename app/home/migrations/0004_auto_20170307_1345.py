# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-07 06:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_auto_20170306_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relatedqueryparameter',
            name='parameter',
            field=models.CharField(choices=[('sale', 'Цена')], help_text='Поле "Параметр" указывает на параметр, \t\tпо которому необходимо сделать выборку', max_length=50, verbose_name='Параметр'),
        ),
    ]
