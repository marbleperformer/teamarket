from __future__ import absolute_import, unicode_literals

from django.db import models

from django.shortcuts import redirect

from wagtail.wagtailcore.models import Page, Orderable

from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel

from wagtail.wagtailcore.fields import RichTextField

from modelcluster.fields import ParentalKey

from catalog.models import CatalogPage

import json

class HomePage(Page):
	# h1_tilte = models.CharField(max_length=250, verbose_name='H1 страницы', blank=True, null=True)

	# description = RichTextField(verbose_name='Описание', blank=True)
	# data_model = models.ForeignKey('catalog.DataModel', verbose_name='Модель данных', on_delete=models.PROTECT)

	# content_panels = Page.content_panels + [
	# 	FieldPanel('description'),
	# 	FieldPanel('data_model'),
	# 	# InlinePanel('home_filters', label='Фильтры'),
	# ]

	# promote_panels = [
	# 	MultiFieldPanel([
	# 		FieldPanel('slug'),
	# 		FieldPanel('h1_tilte'),
	# 		FieldPanel('seo_title'),
	# 		FieldPanel('show_in_menus'),
	# 		FieldPanel('search_description'),
	# 	], 'Общие настройки страниц')
	# ]
	parent_page_types = [
		'wagtailcore.Page'
	]

	def serve(self, request):
		home = self.get_children().type(CatalogPage).first()
		if home:
			return redirect(home.url)
		else:
			home = self.get_children().first()
			return redirect(home.url)

	@property
	def children(self):
		return [json.loads(child.to_json()) for child in self.get_children()]

	# api_fields = ['home_filters', 'children']

	@classmethod
	def can_create_at(cls, parent):
		return super(HomePage, cls).can_create_at(parent) and not cls.objects.exists()


	class Meta:
		verbose_name = 'Главная'

# class RelatedCatalogFilter(Orderable):
# 	page = ParentalKey(HomePage, related_name='home_filters')

# 	filter = models.ForeignKey('catalog.Filter', related_name='home_filter', verbose_name='Фильтр')

# 	@property
# 	def filter_title(self):
# 		return self.filter.title

# 	@property
# 	def filter_field_name(self):
# 		return self.filter.field_name

# 	@property
# 	def filter_options(self):
# 		return self.filter.options_json

# 	api_fields = ['filter_title', 'filter_field_name', 'filter_options']

# 	class Meta:
# 		verbose_name = 'Фильтр'
# 		verbose_name_plural = 'Фильтры'
