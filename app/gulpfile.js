gulp = require('gulp')
uglify = require('gulp-uglify')
concat = require('gulp-concat')
utility = require('gulp-util')
minify = require('gulp-minify-css')

gulp.task('cataloguglifyjs', function() {
	return gulp.src([
			'catalog/static/catalog/js/items/*.js',
			'catalog/static/catalog/js/childs/*.js',
			'catalog/static/catalog/js/filters/*.js',
			'catalog/static/catalog/js/pagination/*.js',

			'catalog/static/catalog/js/router.js',
		])
		.pipe(concat('catalog.min.js'))
		.pipe(uglify())
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('catalog/static/catalog/js/build'))
})

gulp.task('catalogminifycss', function() {
	return gulp.src('catalog/static/catalog/css/*.css')
		.pipe(minify())
		.pipe(concat('catalog.min.css'))
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('catalog/static/catalog/css/build'))
})


gulp.task('teauglifyjs', function() {
	return gulp.src('tea/static/tea/js/items/*.js')
		.pipe(concat('tea.min.js'))
		.pipe(uglify())
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('tea/static/tea/js/build'))
})

gulp.task('teaminifycss', function() {
	return gulp.src('tea/static/tea/css/*.css')
		.pipe(minify())
		.pipe(concat('tea.min.css'))
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('tea/static/tea/css/build'))
})

gulp.task('masteruglifyjs', function() {
	return gulp.src('app/static/js/menu/*js')
		.pipe(concat('master.min.js'))
		.pipe(uglify())
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('app/static/js/build'))
})

gulp.task('masterminifycss', function() {
	return gulp.src([
			'app/static/css/app.css',
			'app/static/css/menu/*.css',
			'app/static/css/footer/*.css'
		])
		.pipe(minify())
		.pipe(concat('master.min.css'))
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('app/static/css/build'))
})

gulp.task('infominifycss', function() {
	return gulp.src('info/static/info/css/*.css')
		.pipe(minify())
		.pipe(concat('info.min.css'))
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('info/static/info/css/build'))
})

gulp.task('cartuglifyjs', function() {
	return gulp.src([
		'cart/static/cart/js/cart/*.js',
		'cart/static/cart/js/router.js'
	])
		.pipe(concat('cart.min.js'))
		.pipe(uglify())
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('cart/static/cart/js/build'))
})

gulp.task('cartminifycss', function() {
	return gulp.src('cart/static/cart/css/*.css')
		.pipe(minify())
		.pipe(concat('cart.min.css'))
		.on('error', function(err) {
			utility.log(utility.colors.red('[Error]'), err.toString());
		})
		.pipe(gulp.dest('cart/static/cart/css/build'))
})
