from django.contrib import admin

from .models import Filter, FilterOption, DataModel

class OptionAdmin(admin.StackedInline):
	model = FilterOption
	extra = 1

class FilterAdmin(admin.ModelAdmin):
	list_display = ['data_model', 'title', 'field_name']

	fieldsets = (
		(None, {'fields':('data_model', 'title', 'field_name')}),
	)

	inlines = [
		OptionAdmin
	]

admin.site.register(Filter, FilterAdmin)
admin.site.register(DataModel)
