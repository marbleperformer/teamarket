from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet 
from rest_framework.filters import DjangoFilterBackend

from .serializers import FilterSerializer

from .models import Filter

class FilterViewSet(ReadOnlyModelViewSet):
	filter_backends = (DjangoFilterBackend,)

	queryset = Filter.objects.all()

	serializer_class = FilterSerializer

	filter_fields = ('id', 'data_model', 'title', 'field_name')
