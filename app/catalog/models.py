from django.db import models

from wagtail.wagtailcore.models import Page, Orderable

from wagtail.wagtailcore.fields import RichTextField

from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel

from modelcluster.fields import ParentalKey

class DataModel(models.Model):
	title = models.CharField(max_length=250, verbose_name='Название')
	page_type = models.CharField(max_length=250, verbose_name='Тип страницы')
	page_fields = models.CharField(max_length=250, verbose_name='Атрибуты страницы')
	limit = models.IntegerField(verbose_name='Количество элементов на странице')

	item_template = models.TextField(blank=True, null=True)

	def __str__(self):
		return self.title
	
	class Meta:
		verbose_name = 'Модель данных'
		verbose_name = 'Модели данных'

class Filter(models.Model):
	data_model = models.ForeignKey(DataModel, verbose_name='Модель данных', related_name='filters')
	title = models.CharField(max_length=250, verbose_name='Название')
	field_name = models.CharField(max_length=250, verbose_name='Название поля')

	def __str__(self):
		return self.title

	@property
	def options_json(self):
		return [option.to_json() for option in self.options.all()]

	class Meta:
		verbose_name = 'Фильтр'
		verbose_name_plural = 'Фильтры'

class FilterOption(models.Model):
	filter = models.ForeignKey(Filter, verbose_name='Фильтр', related_name='options')
	label = models.CharField(max_length=250, verbose_name='Надпись')
	value = models.CharField(max_length=250, verbose_name='Значение')

	def __str__(self):
		return self.value

	def to_json(self):
		return {'label':self.label, 'value':self.value}

	class Meta:
		verbose_name = 'Опция'
		verbose_name_plural = 'Опции'


class CatalogPage(Page):
	h1_title = models.CharField(max_length=250, verbose_name='H1 страницы', blank=True, null=True)

	keywords = models.CharField(max_length=250, verbose_name='Ключевые слова', blank=True, null=True)

	description =RichTextField(verbose_name='Описание', blank=True)
	data_model = models.ForeignKey(DataModel, verbose_name='Модель данных', on_delete=models.PROTECT)

	content_panels = Page.content_panels + [
		FieldPanel('description'),
		FieldPanel('data_model'),
		# InlinePanel('filters', label='Фильтры'),
	]

	promote_panels = [
		MultiFieldPanel([
			FieldPanel('slug'),
			FieldPanel('h1_title'),
			FieldPanel('keywords'),
			FieldPanel('seo_title'),
			FieldPanel('show_in_menus'),
			FieldPanel('search_description'),
		], 'Общие настройки страниц')
	]

	parent_page_types = [
		'home.HomePage',
		'catalog.CatalogPage',
	]

	api_fields = ['url']
	
	class Meta:
		verbose_name = 'Каталог'
		verbose_name_plural = 'Каталоги'

# class RelatedCatalogFilter(Orderable):
# 	page = ParentalKey(CatalogPage, related_name='filters')

# 	filter = models.ForeignKey(Filter, verbose_name='Фильтр')

# 	class Meta:
# 		verbose_name = 'Фильтр'
# 		verbose_name_plural = 'Фильтры'
