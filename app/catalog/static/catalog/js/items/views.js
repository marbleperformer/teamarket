var ItemView = Backbone.View.extend({
	className: 'tile-item col-md-4 col-sm-6 col-xs-12',
	template: _.template($('#tileItemTemplate').html()),

	model:null,

	events: {
		'click .btn-buy': 'add_cart_item',
	},

	add_cart_item: function(event) {
		$(event.target).addClass('in_cart')

		item = cart_collection.get(event.target.name)
		if (!item) {
			item = new Backbone.Model({
				id:event.target.name,
				amount:0
			})
			cart_collection.add(item)
		}
		new_amount = item.get('amount') + 1
		item.set({amount:new_amount})
	},

	render: function() {
		model = this.model.toJSON()
		model.status = cart_collection.get(model.id) ? 'in_cart' : ''
		this.$el.html(
			this.template(model)
		)
		return this
	}
})

var ItemTileView = Backbone.View.extend({
	el:'#tileItems',
	empty_template: _.template($('#itemCollectionEmptyTemplate').html()),

	collection: null,

	initialize: function() {
		// console.log(this.collection)
		this.listenToOnce(this.collection, 'sync', this.render)

		this.collection.fetch()
	},

	render: function(){
		this.$el.empty()
		if (this.collection.length > 0) {
			this.collection.each(function(item) {
				item_view = new ItemView({model:item})
				this.$el.append(item_view.render().el)
			}, this)
		} else {
			this.$el.html(this.empty_template)
		}

		return this
	}
})
