var FilterItemView = Backbone.View.extend({
	tagName: 'li',
	template: _.template($('#filterItemTemplate').html()),
	// option_template: _.template($('#optionTemplate').html()),

	model:null,

	events: {
		'click .btn-filter': 'set_filter',
	},

	set_filter: function(event) {

		options = $('.btn-filter:checked')

		filter_url = options.length != 0 ? 'filters/' : 'page/0'

		options.each(function(index, option) {
			filter_url += '&' + $(option).attr('name') + '=' + $(option).attr('value')
		})

		router.navigate(filter_url, {trigger:true, replace:true})
	},

	render: function() {
		model = this.model.toJSON()

		this.$el.html(this.template(model))
		return this
	}
})

var FilterListView = Backbone.View.extend({
	el:'#filterItems',

	collection: new FilterCollection,

	initialize: function() {
		this.listenToOnce(this.collection, 'sync', this.render)
		this.collection.fetch()
	},

	render: function() {
		this.$el.empty()
		if (this.collection.length > 0) {
			this.$el.html('<h4>ФИЛЬТРЫ</h4>')
		}
		filters_string = this.collection.filters ? this.collection.filters : ''
		this.collection.each(function(item) {
			item.set('filters_string', filters_string )
			item_view = new FilterItemView({model:item})
			this.$el.append(item_view.render().el)
		}, this)

		return this
	}
})