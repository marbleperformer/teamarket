var ChildCollection = Backbone.Collection.extend({
	_url: '',

	url: function() {
		if (this._url) {
			return this._url
		} else {
			return page_parent_url == root_url ? catalog_api_url + '&child_of=' + page_id : catalog_api_url + '&child_of=' + parent_id
		}
	},

	set_url: function(url) {
		this._url = url
	},

	parse: function(response) {
		this.count = response.total_count
		return response.items
	}
})