var ChildItemView = Backbone.View.extend({
	tagName: 'li',

	template: _.template($('#childrenItemtemplate').html()),

	model: null,

	collection: new ChildCollection(),

	initialize: function() {
		this.listenToOnce(this.collection, 'sync', this.render_children)
	},

	render_children: function() {
		this.$el.append('<ul id="child-childitems" class="nav nav-navbar nav-stacked"></ul>')
		this.collection.each(function(item) {
			item_view = new ChildItemView({model:item})
			this.$el.find('ul').append(item_view.render().el)
		})
	},

	render: function() {
		model = this.model.toJSON()
		is_active = page_url.startsWith(model.url)
		model.is_active = is_active ? 'active' : ''
		this.$el.html(
			this.template(model)
		)
		if (is_active) {
			this.collection.set_url(catalog_api_url + '&child_of=' + model.id)
			this.collection.fetch()
		}
		return this
	}
})

var ChildListView = Backbone.View.extend({
	el:'#childItems',

	collection: null,

	initialize: function() {
		this.listenToOnce(this.collection, 'sync', this.render)

		this.collection.fetch() 
	},

	render: function() {
		this.$el.empty()
		if (this.collection.length > 0) {
			this.$el.html('<h4>РАЗДЕЛЫ</h4>')
		}
		this.collection.each(function(item) {
			item_view = new ChildItemView({model:item})
			this.$el.append(item_view.render().el)
		}, this)
		return this
	},
})
