var GroupRouter = Backbone.Router.extend({

	routes: {
		'(/)': 'all',
		'(/)page/:page': 'all',
		'(/)filters/:filters': 'filter',
		'(/)page/:page/filters/:filters': 'filter',
	},

	initialize: function() {
		this.item_collection = new ItemCollection()
		this.filter_collection = new FilterCollection()
		this.child_collection = new ChildCollection()

		child_list_view = new ChildListView({collection: this.child_collection})
	},

	all: function(page) {
		this.item_collection.page = page

		this.item_collection.filters = ''

		this.filter_collection.filters = ''

		item_tile_view = new ItemTileView({collection:this.item_collection})

		filter_list_view = new FilterListView({collection:this.filter_collection})

		pagination_view = new PaginationView({collection: this.item_collection})
	},

	filter: function(filters, page) {
		this.item_collection.page = page

		this.item_collection.filters = filters

		this.filter_collection.filters = filters

		item_tile_view = new ItemTileView({collection:this.item_collection})

		filter_list_view = new FilterListView({collection:this.filter_collection})

		pagination_view = new PaginationView({collection: this.item_collection})
	},
})

router = new GroupRouter()

Backbone.history.start()