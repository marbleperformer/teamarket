# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-09 14:20
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20170409_2117'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='relatedcatalogfilter',
            name='filter',
        ),
        migrations.RemoveField(
            model_name='relatedcatalogfilter',
            name='page',
        ),
        migrations.DeleteModel(
            name='RelatedCatalogFilter',
        ),
    ]
