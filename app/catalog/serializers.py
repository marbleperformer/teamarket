from rest_framework import serializers

from .models import Filter, FilterOption

class OptionSerializer(serializers.ModelSerializer):
	class Meta:
		model = FilterOption
		fields = ['label', 'value']

class FilterSerializer(serializers.ModelSerializer):
	options = OptionSerializer(many=True, read_only=True)

	class Meta:
		model = Filter
		fields = ['title', 'field_name', 'options']
