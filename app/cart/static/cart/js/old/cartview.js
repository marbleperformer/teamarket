PageCartView = Backbone.View.extend({
	el:'#mainPlaceholder',
	main_template: _.template($('#cartViewTemplate').html()),
	item_template: _.template($('#cartItemTemplate').html()),
	empty_template: _.template($('#cartEmptyTeplate').html()),

	items_total_cost:0,

	delivery_cost:0,

	events: {
		'click .btn-remove': 'remove_item',
		'click .btn-increase': 'increase_item_amount',
		'click .btn-decrease': 'decrease_item_amount',
		'change .delivery-selector select': 'set_delivery_cost'
	},

	initialize:function() {
		this.listenTo(cart_collection, 'change:amount', this.set_cart_length)
		this.listenTo(cart_collection, 'remove', this.set_cart_length)
		// this.render()
	},

	increase_item_amount: function(event) {

    	id = $(event.target).attr('name')

		item = cart_collection.get(id)

		amount = item.get('amount')

		amount += 1

		item.set({amount:amount})
    },

    decrease_item_amount: function(event) {

    	id = $(event.target).attr('name')

    	item = cart_collection.get(id)

    	amount = item.get('amount')
		
    	if (amount > 1) {
    		amount -= 1

    		item.set({amount:amount})
    	}
    },

    remove_item: function(event) {

    	id = $(event.target).attr('name')

    	model = cart_collection.get(id)

    	cart_collection.remove(model)
    	$('#' + id).remove()
    },

    set_cart_length: function(model) {
    	amount = model.get('amount')
    	weight = model.get('item').weight * amount
    	item_cost = model.get('item').sale ? model.get('item').sale : model.get('item').cost
    	item_total_cost = item_cost * amount
    	this.items_total_cost = _.reduce(cart_collection.toJSON(), function(memo, num){
    		item_cost = num.item.sale ? num.item.sale : num.item.cost
    		return memo + (num.amount * item_cost)
    	}, 0)

    	$('#' + model.id + ' .amount').html(amount)
    	$('#' + model.id + ' .items-weight').html(weight + ' гр.')
    	$('#' + model.id + ' .items-cost').html(item_total_cost + ' руб.')

    	total_cost = parseFloat(this.items_total_cost)+parseFloat(this.delivery_cost)

    	$('#totalInfo .total-cost b').html(total_cost)

    	$('.delivery-selector select').val(this.delivery_cost)

		if (parseInt(this.items_total_cost) > min_total_cost) {
			$('#checkoutBtn').removeClass('disabled')
		} else {
			$('#checkoutBtn').addClass('disabled')
		}
    },

    set_delivery_cost: function(event, data) {
    	option = $(event.target).find('option:selected')

    	this.delivery_cost = option.attr('value')
    	deliverty_title = option.attr('name')
    	delivery_id = option.attr('id')

    	$('#delveryInfo .cost').html(this.delivery_cost + ' руб.')
    	$('#delveryInfo .title').html(deliverty_title)

    	total_cost = parseFloat(this.items_total_cost)+parseFloat(this.delivery_cost)

    	deliverry_cookies = {id:delivery_id, title:deliverty_title, cost:this.delivery_cost}

    	Cookies.set('delivery_options', deliverry_cookies)

    	$('#totalInfo .total-cost b').html(total_cost)
    },

	render:function() {
		body = ''
		if (cart_collection.length > 0) {
			cart_collection.forEach((el, idx)=>{
				item = el.get('item')

				if (item){
					item.number = idx + 1
					item.amount = el.get('amount')
					item.items_weight = item.weight * item.amount

					item_cost = item.sale ? item.sale : item.cost

					item.item_total_cost = el.get('amount')*item_cost

					this.items_total_cost += item.item_total_cost
					body += this.item_template(item)
				}
			})

			options_string = Cookies.get('delivery_options')

			options = options_string ? JSON.parse(options_string) : null

			delivery_id = options ? options.id : default_delivery_id
			delivery_title = options ? options.title : default_delivery_title
			this.delivery_cost = options ? options.cost : default_delivery_cost

			if (!options) {
				deliverry_cookies = {id:delivery_id, title:delivery_title, cost:this.delivery_cost}
				Cookies.set('delivery_options', deliverry_cookies)
			}

			total_cost = parseFloat(this.items_total_cost) + parseFloat(this.delivery_cost)

			this.$el.html(
				this.main_template({
					body:body, 
					total_cost:total_cost,
					delivery_title: delivery_title,
					delivery_cost: this.delivery_cost + ' руб.'
				})
			)

			$('.delivery-selector select').val(this.delivery_cost)

			if (this.items_total_cost > min_total_cost) {
				$('#checkoutBtn').removeClass('disabled')
			} else {
				$('#checkoutBtn').addClass('disabled')
			}
		} else {
			this.$el.html(this.empty_template())
		}
	}
})

// page_cart_view = new PageCartView()
