var CartRouter = Backbone.Router.extend({
	el:'#mainPlaceholder',

	routes: {
		'(/)': 'cart',
		'(/)client': 'client',
		'(/)back': 'back_to_cart',
	},

	cart: function() {
		view = new PageMainView()
		view.render()
	},

	back_to_cart: function() {
		view = new PageMainView()
		view.render()
	},

	client: function() {
		view = new ClientView()
		view.render()
	}
})

cart_router = new CartRouter()

Backbone.history.start()
