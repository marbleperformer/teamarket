CartItemView = Backbone.View.extend({
	main_placeholder: $('#mainPlaceholder'),

	template: _.template($('#cartItemTemplate').html()),

	empty_template: _.template($('#cartEmptyTeplate').html()),

	tagName: 'div',

	className: 'table-row',

	id: function() {
		return this.model.id
	},

	model: null,

	events: {
		'click .btn-remove': 'remove_item',
		'click .btn-increase': 'increase_item_amount',
		'click .btn-decrease': 'decrease_item_amount',
	},

	initialize: function() {
		this.listenTo(cart_collection, 'change:amount', this.set_cart_length)
		this.listenTo(cart_collection, 'remove', this.set_cart_length)
	},

	increase_item_amount: function(event) {

    	id = $(event.target).attr('name')

		item = cart_collection.get(id)

		amount = item.get('amount')

		amount += 1

		item.set({amount:amount})
    },

    decrease_item_amount: function(event) {

    	id = $(event.target).attr('name')

    	item = cart_collection.get(id)

    	amount = item.get('amount')
		
    	if (amount > 1) {
    		amount -= 1

    		item.set({amount:amount})
    	}
    },

    remove_item: function(event) {

    	id = $(event.target).attr('name')

    	model = cart_collection.get(id)

    	cart_collection.remove(model)
    	$('#' + id).remove()

    	if (cart_collection.length <= 0) {
    		this.main_placeholder.html(this.empty_template())
    	}
    },

	set_cart_length: function(model) {
		item = item_collection.get(model.id)

		amount = model.get('amount')
		weight = item.get('weight') * amount

    	item_cost = item.get('sale') ? item.get('sale') : item.get('cost')
    	item_total_cost = item_cost * amount

    	$('#' + model.id + ' .amount').html(amount)
    	$('#' + model.id + ' .items-weight').html(weight + ' гр.')
    	$('#' + model.id + ' .items-cost').html(item_total_cost + ' руб.')
	},

	render: function() {
		model = this.model.toJSON()
		this.$el.html(
			this.template(model)
		)
		return this
	}
})

PageCartView = Backbone.View.extend({

	tagName:'div',
	id:'cartTable',

	template: _.template($('#mainViewTemplate').html()),

    render:function() {
    	idx = 0
		cart_collection.each(function(el, idx) {
			item = item_collection.get(el.id)

			if (item){
				item.number = idx + 1
				amount = el.get('amount')

				item_sale = item.get('sale')
				item_cost = item.get('cost')

				cost = item_sale ? item_sale : item_cost

				item.set({
					'number':idx + 1,
					'amount':amount,
					'weight':item.get('weight') * amount,
					'cost':cost,
					'total_cost':amount*cost
				})

				item_view = new CartItemView({model:item})

				this.$el.append(item_view.render().el)
			}

		}, this)

		this.$el.append(this.template())

		return this
	}
})

PageMainView = Backbone.View.extend({
	el:"#mainPlaceholder",

	main_placeholder: $('#mainPlaceholder'),

	template: _.template($('#deliverViewTemplate').html()),

	empty_template: _.template($('#cartEmptyTeplate').html()),

	events: {
		'change #deliverySelector select': 'set_delivery_cost'
	},

	initialize: function() {
		this.listenTo(cart_collection, 'change:amount', this.set_cart_length)
		this.listenTo(cart_collection, 'remove', this.set_cart_length)
	},

	get_delivery_options: function() {
		options_string = Cookies.get('delivery_options')

    	options = options_string ? JSON.parse(options_string) : null

		delivery_id = options ? options.id : default_delivery_id
		delivery_title = options ? options.title : default_delivery_title
		delivery_cost = options ? options.cost : default_delivery_cost

		delivery_options = {id:delivery_id, title:delivery_title, cost:delivery_cost}

		if (!options) {
			Cookies.set('delivery_options', delivery_options)
		}

		return delivery_options
	},

	get_items_total_cost: function() {
		return _.reduce(cart_collection.toJSON(), function(memo, num){
    		item = item_collection.get(num.id)
    		sale = item.get('sale')
    		item_cost = sale ? sale : item.get('cost')
    		return memo + (num.amount * item_cost)
    	}, 0)
	},

	set_check_btn_enable: function() {
		if (parseInt(this.get_items_total_cost()) > parseInt(min_total_cost)) {
			$('#checkoutBtn').removeClass('disabled')
		} else {
			$('#checkoutBtn').addClass('disabled')
		}
	},

	set_cart_length: function(model) {
		
    	delivery_cost = this.get_delivery_options().cost

    	total_cost = parseInt(this.get_items_total_cost())+parseInt(delivery_cost)

    	$('#totalInfo .total-cost b').html(total_cost)

    	$('#deliverySelector select').val(delivery_cost)

		this.set_check_btn_enable()
    },

	set_delivery_cost: function(event, data) {
    	option = $(event.target).find('option:selected')

    	delivery_cost = option.attr('value')

    	deliverty_title = option.attr('name')
    	delivery_id = option.attr('id')

    	$('#delveryInfo .cost').html(delivery_cost + ' руб.')
    	$('#delveryInfo .title').html(deliverty_title)

    	total_cost = parseInt(this.get_items_total_cost())+parseInt(delivery_cost)

    	deliverry_cookies = {id:delivery_id, title:deliverty_title, cost:delivery_cost}

    	Cookies.set('delivery_options', deliverry_cookies)

    	$('#totalInfo .total-cost b').html(total_cost)
    },

    render: function() {
    	if (cart_collection.length > 0) {
    		items_view = new PageCartView()

    		delivery_options = this.get_delivery_options()

    		this.$el.append(items_view.render().el)

    		this.$el.append(this.template())

    		$('#delveryInfo .cost').html(delivery_options.cost + ' руб.')
    		$('#delveryInfo .title').html(delivery_options.title)

			total_cost = parseInt(this.get_items_total_cost()) + parseInt(delivery_options.cost)

    		$('#totalInfo .total-cost b').html(total_cost)


			$('#deliverySelector select').val(delivery_options.cost)

			this.set_check_btn_enable()
		} else {
			this.main_placeholder.html(this.empty_template())
		}
		return this
    }
})

ClientView = Backbone.View.extend({
	el:'#mainPlaceholder',
	template:_.template($('#clientViewTemplate').html()),

	events:{
		'click .btn-buy': 'set_customer_cookies'
	},

	set_customer_cookies: function() {
		info = {
			surname: $('#client_surname').val(),
			name: $('#client_name').val(),
			patronymic: $('#client_patronymic').val(),
			phone: $('#client_phone').val(),
			email: $('#client_email').val(),
			address: $('#client_address').val()
		}
		Cookies.set('customer_info', info)
	},

	render: function() {
		this.$el.html(
			this.template()
		)
	}
})