from django.db import models

from django.shortcuts import redirect, get_object_or_404

from django.http import Http404

from wagtail.wagtailcore.models import Page

from .models import Order

import urllib, json

class CartCookiesRequired(object):
	cookies_empty_url = ''

	def dispatch(self, request, *args, **kwargs):
		quote_cart_cookies = request.COOKIES.get('cart_amount')
		quote_delivery_cookies = request.COOKIES.get('delivery_options')

		if not quote_cart_cookies or not quote_delivery_cookies:
			return redirect(self.cookies_empty_url)
		return super(CartCookiesRequired, self).dispatch(request, *args, **kwargs)

class CartNotNullRequired(object):
	cart_empty_url = ''

	def dispatch(self, request, *args, **kwargs):
		quote_cart_cookies = request.COOKIES.get('cart_amount')
		cart_cookies_string = urllib.parse.unquote(quote_cart_cookies)
		cart_cookies = json.loads(cart_cookies_string)

		conditions = models.Q()
		for item in cart_cookies:
			conditions = conditions | models.Q(id=item['id'])

		items_cost = sum(page.specific.cost for page in Page.objects.filter(conditions))

		# items_cost = sum([float(item['item']['cost']) * float(item['amount']) for item in cart_cookies])

		if not items_cost:
			return redirect(self.cart_empty_url)
		return super(CartNotNullRequired, self).dispatch(request, *args, **kwargs)	

class StatusSuccessdRequired(object):
	status_is_not_success = ''

	def dispatch(self, request, *args, **kwargs):
		status = request.GET.get('status')
		cps_context_id = request.GET.get('cps_context_id')
		order = get_object_or_404(Order, cps_context_id=cps_context_id)

		if order.closed:
			raise Http404

		if not cps_context_id and status != 'success':
			return redirect(self.status_is_not_success)
		return super(StatusSuccessdRequired, self).dispatch(request, *args, **kwargs)

class StatusRefusedRequired(object):
	status_is_not_refused = ''

	def dispatch(self, request, *args, **kwargs):
		status = request.GET.get('status')
		cps_context_id = request.GET.get('cps_context_id')
		order = get_object_or_404(Order, cps_context_id=cps_context_id)

		if order.closed:
			raise Http404

		if not cps_context_id and status != 'refused':
			return redirect(self.status_is_not_refused)
		return super(StatusRefusedRequired, self).dispatch(request, *args, **kwargs)
