# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-21 08:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0002_auto_20170321_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartpage',
            name='min_total',
            field=models.DecimalField(blank=True, decimal_places=2, help_text='Поле "Минимальная общая стоимость" не является обязательным.\t\tМинимальная общая стоимость определяет минимальную сумму заказа, доступного к оплате.\t\tМинимальная общая стоимость измеряется в рублях', max_digits=10, null=True, verbose_name='Минимальная общая стоимость'),
        ),
    ]
