# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-04 06:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0004_auto_20170322_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartpage',
            name='min_total',
            field=models.IntegerField(blank=True, help_text='Поле "Минимальная общая стоимость" не является обязательным.\t\tМинимальная общая стоимость определяет минимальную сумму заказа, доступного к оплате.\t\tМинимальная общая стоимость измеряется в рублях', null=True, verbose_name='Минимальная общая стоимость'),
        ),
        migrations.AlterField(
            model_name='relateddeliverymethod',
            name='cost',
            field=models.IntegerField(default=0, help_text='Поле "Стоимость доставки" не является обязательным.\t\tСтоимость доставки измеряется в рублях', verbose_name='Стоимость доставки'),
        ),
    ]
