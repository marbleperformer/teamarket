from django.db import models

from wagtail.wagtailcore.models import Page, Orderable

from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel

from wagtail.wagtailcore.fields import RichTextField

from wagtail.contrib.wagtailapi import serializers

from modelcluster.fields import ParentalKey

from customers.forms import CustomerForm

import json, urllib

class CartPage(Page):
	message_text = RichTextField( 
		verbose_name='Текст почтового сообщения',
		blank=True,
		null=True, 
		help_text='Текст сообщения, которое будет отправлено пользователю, после совершения покупки. В случае если оплата покупки не состоялась, сообщение отправляться не будет'
	)

	success_text = RichTextField(
		verbose_name='Текст "Оплата прошла успешно"', 
		blank=True,
		null=True,
		help_text='Текст сообщения, которое появится на сайте в случае состоявшейся оплаты заказа'
	)

	fail_text = RichTextField(
		verbose_name='Текст "Оплата не состоялась"', 
		blank=True,
		null=True,
		help_text='Текст сообщения, которое появится на сайте в случае неудачной попытки оплаты заказа'
	)

	min_total_cost = models.IntegerField(
		verbose_name='Минимальная общая стоимость',
		blank=True,
		null=True,
		help_text='Поле "Минимальная общая стоимость" не является обязательным.\
		Минимальная общая стоимость определяет минимальную сумму заказа, доступного к оплате.\
		Минимальная общая стоимость измеряется в рублях'
	)

	content_panels = Page.content_panels + [
		FieldPanel('min_total_cost'),
		MultiFieldPanel([
			FieldPanel('message_text'),
			FieldPanel('success_text'),
			FieldPanel('fail_text')
		], 'Сообщения'),
		InlinePanel('delivery_methods', label='Методы доставки'),
	]

	promote_panels = [
		MultiFieldPanel([
			FieldPanel('seo_title'),
			FieldPanel('show_in_menus'),
			FieldPanel('search_description'),
		], 'Общие настройки страниц')
	]

	parent_page_types = [
		'home.HomePage'
	]

	subpage_types = [
	]

	@classmethod
	def can_create_at(cls, parent):
		return super(CartPage, cls).can_create_at(parent) and not cls.objects.exists()

	def get_context(self, request):
		context = super(CartPage, self).get_context(request)		

		context['customer_form'] = CustomerForm()

		quote_cart_cookies = request.COOKIES.get('cart_amount')
		if quote_cart_cookies:

			cart_cookies_string = urllib.parse.unquote(quote_cart_cookies)
			cart_cookies = json.loads(cart_cookies_string)
			
			conditions = models.Q()
			for item in cart_cookies:
				conditions = conditions | models.Q(id=item['id'])

			item_collection = list()

			if conditions:
				for page in Page.objects.filter(conditions):
					spec = page.specific
					item_json = json.loads(spec.to_json())
					item_json['image_url'] = spec.image_url
					item_json['image_alt'] = spec.image_alt
					item_json['url'] = spec.url
					item_json['id'] = spec.pk

					item_collection.append(item_json)

		context['items'] =  json.dumps(item_collection) if quote_cart_cookies else '{}'

		return context

	def save(self, *args, **kwargs):
		self.slug = 'cart'
		super(CartPage, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'Корзина'
		verbose_name_plural = 'Корзина'

class RelatedDeliveryMethod(Orderable):
	page = ParentalKey(CartPage, related_name='delivery_methods')

	title = models.CharField(
		max_length=50,
		verbose_name='Метод доставки',
		unique=True)

	cost = models.IntegerField(
		verbose_name='Стоимость доставки',
		default=0,
		help_text='Поле "Стоимость доставки" не является обязательным.\
		Стоимость доставки измеряется в рублях')

	panels = [
		FieldPanel('title'),
		FieldPanel('cost')
	]

	def __str__(self):
		return self.title

class Order(models.Model):
	cps_context_id = models.CharField(max_length=250)

	amount = models.TextField(verbose_name='Количество')

	customer = models.ForeignKey('customers.Customer', verbose_name='Покупатель')

	delivery = models.ForeignKey(RelatedDeliveryMethod, verbose_name='Способ доставки')

	items = models.ManyToManyField('wagtailcore.Page', verbose_name='Список покупок')

	closed = models.DateTimeField(blank=True, null=True)

	@property
	def total(self):
		amount = json.loads(self.amount)
		return sum([amount[str(item.id)] * (item.specific.sale if item.specific.sale else item.specific.cost) for item in self.items.all()]) + self.delivery.cost

	class Meta:
		verbose_name = 'Заказ'
		verbose_name_plural = 'Заказы'
