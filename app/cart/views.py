from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View

from django.template.loader import render_to_string

from django.conf import settings

from django.contrib.auth.models import Group

from django.utils import timezone

from .mixins import CartCookiesRequired, CartNotNullRequired, StatusSuccessdRequired, StatusRefusedRequired

from customers.forms import CustomerForm

from customers.models import Customer

from .models import Order

from .utils import get_yandex_result, send_mail_to, Mail

from .models import CartPage

import urllib, json, smtplib

class PaymentView(CartCookiesRequired, CartNotNullRequired, View):
	cookies_empty_url = '/cart/#back'
	cart_empty_url = '/cart/#back'

	def post(self, request):
		customer_form = CustomerForm(request.POST)

		customer = None

		if customer_form.is_valid():
			customer = Customer.objects.get_or_create(**customer_form.cleaned_data)[0]

		quote_cart_cookies = request.COOKIES.get('cart_amount')
		cart_cookies_string = urllib.parse.unquote(quote_cart_cookies)
		cart_cookies = json.loads(cart_cookies_string)

		quote_delivery_cookies = request.COOKIES.get('delivery_options')
		delivery_cookies_string = urllib.parse.unquote(quote_delivery_cookies)
		delivery_cookies = json.loads(delivery_cookies_string)

		item_id_list = list()
		item_amount_dict = dict()

		for item in cart_cookies:
			item_amount = item['amount']

			item_id_list.append(item['id'])
			item_amount_dict[str(item['id'])] = item['amount']
			
		amount = json.dumps(item_amount_dict)
		delivery_method_id = int(delivery_cookies['id'])

		order = Order.objects.create(amount=amount, delivery_id=delivery_method_id, customer=customer)

		
		order.items.add(*item_id_list)	

		result = get_yandex_result(order.total)

		params = result['acs_params']

		order.cps_context_id = params['cps_context_id']
		order.save()

		param_strings = ['{}={}'.format(key,value) for key, value in params.items()]

		ynadex_url = result['acs_uri'] + '?' + '&'.join(param_strings)

		return redirect(ynadex_url)

class PaymentSuccessView(StatusSuccessdRequired, View):
	context_is_none = '/'

	def get(self, request, *args, **kwargs):
		page = CartPage.objects.first()
		cps_context_id = request.GET.get('cps_context_id')
		order = get_object_or_404(Order, cps_context_id=cps_context_id)

		item_context_list = list()
		amount_dict = json.loads(order.amount)

		for item in order.items.all():
			item_object = item.specific
			amount = amount_dict[str(item.id)]
			item_context_list.append({
				'title':item_object.title,
				'short':item_object.short,
				'weight':item_object.weight * amount,
				'image':item_object.image,
				'cost':(item_object.sale if item_object.sale else item_object.cost) * amount,
				'url':settings.DEFAULT_SITE_DOMAIN + item_object.url,
			})

		message_text = render_to_string('cart/message_template.html', {'id':order.id,'customer':order.customer, 'items':item_context_list, 'total': order.total, 'delivery':order.delivery})

		customer_message_text = render_to_string('cart/customer_message_template.html', {'id':order.id,'items': item_context_list, 'total': order.total, 'delivery': order.delivery, 'message_text': page.message_text})

		Moderators = Group.objects.get(name='Moderators')
		emails = [user.email for user in Moderators.user_set.all()]

		customer_message = Mail(subject='Заказ оформлен', text=customer_message_text, from_address='info@teachina24.ru', to_address=[order.customer.email])

		moderators_message = Mail(subject='Заказ оформлен', text=message_text, from_address='info@teachina24.ru', to_address=emails)

		order.closed = timezone.now()
		order.save()

		context = {'page':page, 'items':item_context_list}

		send_mail_to(customer_message, moderators_message)

		return render(request, 'cart/success_page.html', context)

class PaymentFailView(StatusRefusedRequired, View):
	status_is_none = '/'

	def get(self, request):
		page = CartPage.objects.first()
		order = get_object_or_404(Order, cps_context_id=cps_context_id)

		order.closed = timezone.now()
		order.save()

		return render(request, 'cart/fail_page.html', {'page':page})
