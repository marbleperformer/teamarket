from django.conf.urls import url

from .views import PaymentView, PaymentSuccessView, PaymentFailView

app = 'cart'

urlpatterns = [
	url(r'^$', PaymentView.as_view(), name='payment'),
	url(r'^success/$', PaymentSuccessView.as_view(), name='success'),
	url(r'^refused/$', PaymentFailView.as_view(), name='refused'),
]
