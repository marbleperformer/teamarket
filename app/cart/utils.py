# from django.shortcuts import redirect

from wagtail.wagtailcore.models import Page

from django.conf import settings

from yandex_money import api

from email.mime.text import MIMEText

import smtplib

class Mail(object):
	def __init__(self, **kwargs):
		self.__dict__.update(**kwargs)

def get_yandex_result(amount_due):
	instance_response = api.ExternalPayment.get_instance_id(settings.APP_KEY)

	instance_id = instance_response['instance_id']

	external_payment = api.ExternalPayment(instance_id)

	payment_options = {
		'pattern_id':'p2p',
		'instance_id':instance_id,
		'to':settings.YANDEX_WALLET_ID,
		'amount':amount_due
	}

	external_response = external_payment.request(payment_options)

	request_id = external_response['request_id']

	process_options = {
		'request_id':request_id,
		'instance_id':instance_id,
		'ext_auth_success_uri':settings.DEFAULT_SITE_DOMAIN + '/payment/success/',
		# 'ext_auth_success_uri':'http://teachina24.ru/catalog/',
		'ext_auth_fail_uri':settings.DEFAULT_SITE_DOMAIN + '/payment/refused/'
		# 'ext_auth_fail_uri':'http://teachina24.ru/catalog/'
	}

	return external_payment.process(process_options)

	# params = result['acs_params']

	# param_strings = ['{}={}'.format(key,value) for key, value in params.items()]

	# ynadex_url = result['acs_uri'] + '?' + '&'.join(param_strings) 

	# return ynadex_url, params

def order_to_html(order):
	order.items.all()

def send_mail_to(*messages):

	server = smtplib.SMTP_SSL('smtp.yandex.ru:465')

	server.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
	for message in messages:
		msg =  MIMEText(message.text, _subtype='html', _charset='utf-8')
		msg['Subject'] = message.subject
		msg['From'] = message.from_address
		msg['To'] = str(message.to_address)

		server.sendmail(settings.EMAIL_HOST_USER, message.to_address, msg.as_string())

	server.quit()

