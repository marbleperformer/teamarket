from django.db import models

from django.core.validators import MinValueValidator, MaxLengthValidator

from wagtail.wagtailcore.models import Page

from wagtail.wagtailcore.fields import RichTextField

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from modelcluster.fields import ParentalKey	

from .utils import generate_image_url

class TeaPage(Page):
	h1_tilte = models.CharField(max_length=250, verbose_name='H1 страницы', blank=True, null=True)

	keywords = models.CharField(max_length=250, verbose_name='Ключевые слова', blank=True, null=True)

	short = models.TextField(
		verbose_name='Краткое описание',
		validators=[MaxLengthValidator(100)],
		help_text='Содержимое поля "Краткое описание" будет отображаться\
		в каталоге продукции')
	description = RichTextField(
		verbose_name='Описание', 
		help_text='Содержимое поля "Описание" будет отображаться\
		в карточке товара')
	cost = models.IntegerField(
		verbose_name='Цена', 
		validators=[MinValueValidator(0.01)], 
		help_text='Поле "Цена" является обязательным.\
		Цена соответствует весу порции.\
		Цена измеряется в рублях')
	sale = models.IntegerField(
		verbose_name='Цена со скидкой',
		null=True, 
		blank=True,
		validators=[MinValueValidator(0.01)], 
		help_text='Содержимое поля "Цена со скидкой" будет отображаться\
		в карточке товара и в каталоге продукции.\
		В случае если это значение будет задано, \
		стоимость порции будет расчитываться исходя из значения этого поля')
	weight = models.IntegerField(
		verbose_name='Вес порции',
		validators=[MinValueValidator(0.001)],
		help_text='Поле "Вес порции" является обязательным.\
		Вес порции измеряется в граммах')
	image = models.ForeignKey(
		'wagtailimages.Image', 
		verbose_name='Изображение',
		on_delete=models.PROTECT,
		help_text='Поле "Изображение" является обязательным.\
		Изображение будет отображаться\
		в карточке товара и в каталоге продукции')

	_is_sale = models.BooleanField(db_column='is_sale', default=False)

	modified = models.DateField(verbose_name='Дата изменения', auto_now_add=True)
	created = models.DateField(verbose_name='Дата создание', auto_now_add=True)

	content_panels = [
		MultiFieldPanel([
			FieldPanel('title'),
			ImageChooserPanel('image'),

		], 'Информация'),
		MultiFieldPanel([
			FieldPanel('description'),
			FieldPanel('weight'),
			FieldPanel('cost'),
		], 'Параметры'),
		MultiFieldPanel([
			FieldPanel('short'),
			FieldPanel('sale'),
		], 'Дополнительные параметры')
	]

	promote_panels = [
		MultiFieldPanel([
			FieldPanel('slug'),
			FieldPanel('h1_tilte'),
			FieldPanel('keywords'),
			FieldPanel('seo_title'),
			FieldPanel('show_in_menus'),
			FieldPanel('search_description'),
		], 'Общие настройки страниц')
	]

	parent_page_types = [
		'catalog.CatalogPage',
	]

	subpage_types = [
	]

	api_fields = ['url', 'sale', 'cost', 'short', '_is_sale', 'image_url', 'image_alt', 'weight']

	@property
	def is_sale(self):
		return self._is_sale

	@property
	def image_url(self):
		return generate_image_url(self.image, 'fill-240x240')

	@property
	def image_alt(self):
		return self.image.title

	def save(self, *args, **kwargs):
		self._is_sale = bool(self.sale)
		super(TeaPage, self).save(*args, **kwargs)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Чай'
		verbose_name_plural = 'Чаи'
