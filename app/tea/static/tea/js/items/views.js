var TeaDetailView = Backbone.View.extend({
	el:'#buyPanel',
	template:_.template($('#buyPanelTemplate').html()),

	model: new ItemModel(),

	value:1,
	value_el: '.number',

	events: {
		'click .btn-buy': 'add_cart_item',
		'click .btn-increase': 'increase_item_number',
		'click .btn-decrease': 'decrease_item_number'
	},

	initialize:function() {
		this.model.fetch()
		this.render()
	},

	add_cart_item: function(event) {
		$(event.target).addClass('in_cart')

		item = cart_collection.get(event.target.name)
		if (!item) {
			item = new Backbone.Model({
				id:event.target.name,
				item:this.model,
				amount:0
			})
			cart_collection.add(item)
		}
		new_amount = item.get('amount') + this.value
		item.set({amount:new_amount})
	},

	increase_item_number: function() {
        this.value += 1
        this.$el.find(this.value_el).html(this.value)
    },

    decrease_item_number: function() {
    	if (this.value > 1) {
    		this.value -= 1
    		this.$el.find(this.value_el).html(this.value)
    	}
    },

	render: function() {
		status = cart_collection.get(item_id) ? 'in_cart' : ''
		this.$el.html(this.template({value:this.value, status:status}))
		return this
	}
})

tea_view = new TeaDetailView()
