from django.core.urlresolvers import reverse
import wagtail.wagtailimages as images_lib

def generate_image_url(image, filter_spec):
    signature = images_lib.views.serve.generate_signature(image.id, filter_spec)
    url = reverse('wagtailimages_serve', args=(signature, image.id, filter_spec))

    url += image.file.name[len('original_images/'):]

    return url